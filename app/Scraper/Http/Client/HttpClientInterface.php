<?php


namespace App\Scraper\Http\Client;


use Exception;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Interface for Minimal Http Client which can only download content from urls
 *
 * Interface HttpClientInterface
 * @package App\Scraper\Http\Client
 */
interface HttpClientInterface
{

    /**
     * Set url to get
     *
     * @param $url
     * @return HttpClientInterface
     */
    public function setUrl(string $url): HttpClientInterface;

    /**
     * Sets cache interval
     *
     * @param int $seconds
     * @return HttpClientInterface
     */
    public function setCacheTtl(int $seconds): HttpClientInterface;

    /**
     * Get response body for the given url
     *
     * @return string
     */
    public function getResponseBody(): string;

}
