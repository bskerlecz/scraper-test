<?php


namespace App\Scraper\Sources\Sainsbury;

use App\Models\Product;
use App\Scraper\Http\HttpClientFactory;
use App\Scraper\ProcessorInterface;
use App\Scraper\Sources\Sainsbury\Parsers\CategoryPageParser;
use App\Scraper\Sources\Sainsbury\Parsers\ParserException;
use App\Scraper\Sources\Sainsbury\Parsers\ProductPageParser;
use Illuminate\Support\Facades\Log;

/**
 * Class CategoryProductsProcessor
 *
 * Processes a category page from the Sainsbury site and extract information about the listed products
 *
 * @package App\Scraper\Sources\Sainsbury
 */
class CategoryProductsProcessor implements ProcessorInterface
{

    const CLIENT_CACHE_TTL = 600 * 30; // cache pages for 30 min

    protected $url;

    /**
     * @var HttpClientFactory
     */
    protected $httpClientFactory;

    /**
     * CategoryProductsProcessor constructor.
     * @param HttpClientFactory $httpClientFactory
     */
    public function __construct(HttpClientFactory $httpClientFactory)
    {
        $this->httpClientFactory = $httpClientFactory;
    }

    public function setUrl(string $url): ProcessorInterface
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Returns list of the products found on a given url (Sainsbury category page)
     *
     * @return array
     * @throws ParserException
     */
    public function getResult(): array
    {
        $categoryPageHtml = $this->getPage();
        $productUrls = $this->getProductPageUrlsFromCategoryPage($categoryPageHtml);

        $products = [];
        foreach ($productUrls as $productUrl) {
            $html = $this->getPageWithBrowser($productUrl);
            $product = $this->getProductInfo($html);
            $products[] = $product;
        }

        return $products;
    }

    /**
     * Returns the main page html
     *
     * @return string
     * @throws ParserException
     */
    protected function getPage(): string
    {
        $downloader = $this->httpClientFactory->create(false);
        $html = $downloader->setUrl($this->url)
            ->setCacheTtl(self::CLIENT_CACHE_TTL)
            ->getResponseBody();

        if (empty($html)) {
            Log::warning("Cannot parse category page from '{$this->url}', got empty or no response.", [__METHOD__]);
            throw new ParserException();
        }
        return $html;
    }
    /**
     * Returns the page html from the given url using javascript enabled browser
     *
     * @param $url
     * @return string
     * @throws ParserException
     */
    protected function getPageWithBrowser($url): string
    {
        $downloader = $this->httpClientFactory->create(true); // use full featured javascript enabled browser so the page is rendered correctly
        $html = $downloader->setUrl($url)
            ->setCacheTtl(self::CLIENT_CACHE_TTL)
            ->getResponseBody();
        if (empty($html)) {
            Log::warning("Cannot parse product page from '{$this->url}', got empty or no response.", [__METHOD__]);
            throw new ParserException();
        }
        return $html;
    }

    /**
     * Extract product urls from the given html
     *
     * @param $html
     * @return array
     * @throws ParserException
     */
    protected function getProductPageUrlsFromCategoryPage($html): array
    {
        $parser = new CategoryPageParser($html);
        $productPageUrls = $parser->getProductPageUrls();
        if (empty($productPageUrls)) {
            Log::warning("No products found on '{$this->url}'", [__METHOD__]);
            throw new ParserException();
        }
        return $productPageUrls;
    }

    /**
     * Extracts product information from the product page html into the Product model
     *
     * @param $html
     * @return Product
     * @throws ParserException
     */
    protected function getProductInfo($html): Product {
        $parser = new ProductPageParser($html);
        $product = $parser->getProduct();
        if (empty($product)) {
            Log::warning("Cannot parse product page.", [__METHOD__]);
            throw new ParserException();
        }
        return $product;
    }

}
