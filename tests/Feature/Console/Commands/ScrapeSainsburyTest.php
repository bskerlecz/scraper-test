<?php

namespace Tests\Feature\Console\Commands;

use App\Scraper\Http\Client\HttpClientInterface;
use App\Scraper\Http\HttpClientFactory;
use Tests\ReadFixtureFile;
use Tests\TestCase;

/**
 * Class ScrapeSainsburyTest
 *
 * Tests the scraper command with data loaded from fixtures
 *
 * @package Tests\Feature\Console\Commands
 */
class ScrapeSainsburyTest extends TestCase
{

    use ReadFixtureFile;

    protected $categoryHtml;

    protected $productHtml = [
        'example_product_1.html' => '',
        'example_product_2.html' => '',
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $this->categoryHtml = $this->readFixtureFile('scrape/example_category.html');

        foreach (array_keys($this->productHtml) as $key) {
            $this->productHtml[$key] = $this->readFixtureFile('scrape/' . $key);
        }

        // Create mocked httpclient which returns page html-s from the fixtures directory
        $client = $this->mock(HttpClientInterface::class);
        $client->shouldReceive('setUrl')
            ->andReturnUsing(function($arg) use ($client) {
                $client->url = $arg;
                return $client;
            });

        $client->shouldReceive('setCacheTtl')
            ->andReturnUsing(function($arg) use ($client) {
                return $client;
            });

        $client->shouldReceive('getResponseBody')
            ->andReturnUsing(function() use ($client) {
                if (!empty($this->productHtml[$client->url])) {
                    return $this->productHtml[$client->url];
                } else {
                    return $this->categoryHtml;
                }
            });

        // Use the mocked HttpClient in HttpClientFactory
        $this->mock(HttpClientFactory::class, function($mock) use ($client) {
            $mock->shouldReceive('create')
                ->andReturnUsing(function ($arg) use ($client) {
                    return $client;
                });
        });
    }

    public function testScrape()
    {
        $this->artisan('scrape:sainsbury --raw')
            ->expectsOutput('{"results":[{"title":"Product 1","unitPrice":"12.25","description":"Product 1 is the best product","size":"0.4kb"},{"title":"Product 2","unitPrice":"2.25","description":"Product 2 is also the best product","size":"0.4kb"}],"total":14.5}')
            ->assertExitCode(0);
    }
}
