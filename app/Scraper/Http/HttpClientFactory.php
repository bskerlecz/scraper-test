<?php


namespace App\Scraper\Http;


use App\Scraper\Http\Client\BrowserHttpClient;
use App\Scraper\Http\Client\HttpClientInterface;
use App\Scraper\Http\Client\SimpleHttpClient;

/**
 * Class HttpClientFactory
 *
 * Class to create basic http clients
 *
 * @package App\Scraper\Http
 */
class HttpClientFactory
{

    /**
     * Returns a simple http client which can be used to download content from given url
     *
     * @param bool $javascriptEnabled Whether to enable javascript execution or not
     * @return HttpClientInterface
     */
    public function create(bool $javascriptEnabled): HttpClientInterface
    {
        if ($javascriptEnabled) {
            return new BrowserHttpClient();
        } else {
            return new SimpleHttpClient();
        }
    }

}
