<?php


namespace App\Scraper;


interface ScraperInterface
{

    /**
     * Sets the url to scrape
     *
     * @param string $url
     * @return ScraperInterface
     */
    public function setUrl(string $url): ScraperInterface;

    /**
     * Sets the processor which will be used to generate the result
     *
     * @param string $class
     * @return ScraperInterface
     */
    public function setProcessor(string $class): ScraperInterface;

    /**
     * Returns the scraping result
     *
     * @return mixed
     */
    public function getResult();

}
