<?php


namespace Tests;


trait ReadFixtureFile
{
    public function readFixtureFile($name) {
        $file = __DIR__ . "/_fixtures/{$name}";
        if (!file_exists($file)) {
            return false;
        }
        return file_get_contents($file);
    }
}
