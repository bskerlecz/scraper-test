<?php

namespace App\Console\Commands;

use App\Scraper\ScraperInterface;
use Illuminate\Console\Command;

/**
 * Class ScrapeSainsbury
 *
 * Command to get Sainsbury Ripe products page and output the product list as a JSON
 *
 * @package App\Console\Commands
 */
class ScrapeSainsbury extends Command
{
    protected $url = 'https://www.sainsburys.co.uk/shop/gb/groceries/fruit-veg/ripe---ready#langId=44&storeId=10151&catalogId=10241&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=60&orderBy=FAVOURITES_ONLY%7CTOP_SELLERS&searchTerm=&beginIndex=0&hideFilters=true';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:sainsbury {--raw : Output raw json instead of formatted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes and parses Sainsbury\'s Ripe products category page into product listing json';

    /** @var ScraperInterface */
    protected $scraper;

    /**
     * Create a new command instance.
     * @param ScraperInterface $scraper
     */
    public function __construct(ScraperInterface $scraper)
    {
        parent::__construct();
        $this->scraper = $scraper;
    }

    /**
     * Format and output product list json
     *
     * @param $products
     * @param $jsonOpts
     */
    protected function outputProductsJson($products, $jsonOpts) {
        $result = [];

        if (!empty($products)) {
            $priceTotal = 0;
            foreach ($products as $product) {
                $productData = $product->toArray();
                $priceTotal += $productData['unitPrice'];
                $productData['unitPrice'] = number_format($productData['unitPrice'],2);
                $productData['size'] = round($productData['size'] / 1024, 1) . 'kb';
                $result['results'][] = $productData;
            }
            $result['total'] = $priceTotal;
        }

        $this->line(json_encode($result, $jsonOpts));
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $outputFormatted = !$this->option('raw');
        $jsonOpts = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | ($outputFormatted ? JSON_PRETTY_PRINT : 0);

        $products = $this->scraper
            ->setUrl($this->url)
            ->setProcessor(\App\Scraper\Sources\Sainsbury\CategoryProductsProcessor::class)
            ->getResult();

        $this->outputProductsJson($products, $jsonOpts);

        return 0;
    }
}
