<?php


namespace App\Scraper\Http\Client;


use App\Scraper\Common\MissingParameterException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/**
 * Class SimpleHttpClient
 *
 * Simple http client for getting plain html pages in fast and resource friendly way
 *
 * @package App\Scraper\Http\Client
 */
class SimpleHttpClient implements HttpClientInterface
{

    protected $url;
    protected $cacheTtl = 0;

    public function setUrl(string $url): HttpClientInterface
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @throws MissingParameterException
     */
    public function getResponseBody(): string
    {
        if (empty($this->url)) {
            throw new MissingParameterException('url');
        }

        if (Cache::has($this->url)) {
            return Cache::get($this->url);
        }

        Log::debug("Fetching url '{$this->url}'", [__METHOD__]);

        $response = Http::get($this->url);
        if ($response->successful()) {
            $html = $response->body();

            if ($this->cacheTtl>0) {
                Cache::put($this->url, $html, $this->cacheTtl);
            }

            return $html;
        }

        Log::warning("Cannot fetch page from url '{$this->url}', server replied with status: {$response->status()}", [__METHOD__]);
        return false;
    }

    public function setCacheTtl(int $seconds): HttpClientInterface
    {
        $this->cacheTtl = $seconds;
        return $this;
    }

}
