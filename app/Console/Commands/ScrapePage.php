<?php

namespace App\Console\Commands;

use App\Scraper\Http\HttpClientFactory;
use Illuminate\Console\Command;

/**
 * Class ScrapePage
 *
 * Command to get page from a given url
 *
 * @package App\Console\Commands
 */
class ScrapePage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:page {url} {--javascript : enable javascript execution on the page}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes a page from the given url and write it to standard output';

    /**
     * @var HttpClientFactory
     */
    protected $httpClientFactory;

    /**
     * Create a new command instance.
     *
     * @param HttpClientFactory $httpClientFactory
     */
    public function __construct(HttpClientFactory $httpClientFactory)
    {
        parent::__construct();
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $url = $this->argument('url');
        $javascriptEnabled = $this->option('javascript');

        $html = $this
            ->httpClientFactory
            ->create($javascriptEnabled)
            ->setUrl($url)
            ->getResponseBody();

        $this->line($html);

        return 0;
    }
}
