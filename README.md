Scraper for Sainsbury Ripe products

Requirements:
* PHP 7.3+
* Composer (see `https://getcomposer.org/download/`)
* Headless chrome working (can be checked by executing  `./node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome` after installation)

Install:
* `composer install` - PHP dependencies 
* `npm install` - Node dependencies including headless Chrome (Puppeteer lib.)

Running tests:
* `php artisan test`

Executing command:
* `php artisan scrape:sainsbury`
