<?php


namespace App\Scraper;


use App\Scraper\Common\InvalidArgumentException;
use App\Scraper\Common\MissingParameterException;
use Illuminate\Support\Facades\App;

class Scraper implements ScraperInterface
{

    /**
     * @var ProcessorInterface
     */
    protected $processor;

    protected $url;

    /**
     * Scraper constructor.
     */
    public function __construct()
    {
    }

    /**
     * @inheritdoc
     */
    public function setUrl(string $url): ScraperInterface
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    public function setProcessor(string $class): ScraperInterface
    {
        try {
            $instance = app($class);
            if (!$instance instanceof ProcessorInterface) {
                throw new \Exception("Invalid type.");
            }
        } catch (\Exception $e) {
            throw new InvalidArgumentException("'class' must be a class name and must implement " . ProcessorInterface::class);
        }

        $this->processor = $instance;
        return $this;
    }

    /**
     * @inheritdoc
     * @throws MissingParameterException
     */
    public function getResult()
    {
        if (empty($this->url)) {
            throw new MissingParameterException('url');
        }
        if (empty($this->processor)) {
            throw new MissingParameterException('processor');
        }

        return $this
            ->processor
            ->setUrl($this->url)
            ->getResult();
    }
}
