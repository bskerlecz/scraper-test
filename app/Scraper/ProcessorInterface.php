<?php


namespace App\Scraper;


interface ProcessorInterface
{
    /**
     * Set url to process
     *
     * @param string $url
     * @return ProcessorInterface
     */
    public function setUrl(string $url) : ProcessorInterface;

    /**
     * Processed result
     * @return mixed
     */
    public function getResult();
}
