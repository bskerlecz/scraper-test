<?php


namespace App\Scraper\Sources\Sainsbury\Parsers;


use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class CategoryPageParser
 *
 * Parse product page urls from Sainsbury category page
 *
 * @package App\Scraper\Sources\Sainsbury\Parsers
 */
class CategoryPageParser
{
    protected $html;
    protected $urls;

    /**
     * CategoryPageParser constructor.
     * @param $html
     */
    public function __construct($html)
    {
        $this->html = $html;
    }

    /**
     * Parse and return list of product page urls
     *
     * @return array
     */
    public function getProductPageUrls(): array
    {
        if ($this->urls == null) {
            $crawler = new Crawler();
            $crawler->addHtmlContent($this->html);

            $this->urls = [];

            try {
                $productLinks = $crawler->filter('.product .productInfo h3 > a');
                if ($productLinks->count()) {
                    for ($i = 0; $i < $productLinks->count(); $i++) {
                        $a = $productLinks->eq($i);
                        $href = $a->attr('href');
                        $this->urls[] = $href;
                    }
                }
            } catch (\Exception $e) {
                Log::warning("Page parsing failed: '{$e->getMessage()}' - {$e->getTraceAsString()}", [__METHOD__]);
                $this->urls = [];
            }
        }

        return $this->urls;
    }

}
