<?php


namespace App\Scraper\Http\Client;


use App\Scraper\Common\MissingParameterException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Spatie\Browsershot\Browsershot;

/**
 * Class BrowserHttpClient
 *
 * Heavy http client with an ability to execute javascript - it uses Chromium browser as the backend
 *
 * @package App\Scraper\Http\Client
 */
class BrowserHttpClient implements HttpClientInterface
{

    protected $url;
    protected $cacheTtl = 0;

    public function setUrl(string $url): HttpClientInterface
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @throws MissingParameterException
     */
    public function getResponseBody(): string
    {
        if (empty($this->url)) {
            throw new MissingParameterException('url');
        }

        if (Cache::has($this->url)) {
            return Cache::get($this->url);
        }

        try {
            Log::debug("Fetching url '{$this->url}'", [__METHOD__]);

            $html = Browsershot::url($this->url)
                ->waitUntilNetworkIdle()
                ->timeout(60)
                ->bodyHtml();

            if (!empty($html)) {
                if ($this->cacheTtl>0) {
                    Cache::put($this->url, $html, $this->cacheTtl);
                }

                return $html;
            }
        } catch (\Exception $e) {
            Log::warning("Cannot fetch page from url '{$this->url}', exception: {$e->getMessage()} - {$e->getTraceAsString()}", [__METHOD__]);
        }

        return false;
    }

    public function setCacheTtl(int $seconds): HttpClientInterface
    {
        $this->cacheTtl = $seconds;
        return $this;
    }

}
