<?php

namespace Tests\Unit\Scraper\Sources\Sainsbury\Parsers;

use App\Models\Product;
use App\Scraper\Sources\Sainsbury\Parsers\ProductPageParser;
use PHPUnit\Framework\TestCase;
use Tests\ReadFixtureFile;

/**
 * Class ProductPageParserTest
 *
 * Sainsbury Ripe Product page parser test
 *
 * @package Tests\Unit\Scraper\Sources\Sainsbury\Parsers
 */
class ProductPageParserTest extends TestCase
{
    use ReadFixtureFile;

    public function testGetProductSnapshotDescription()
    {
        $html = $this->readFixtureFile('product_page_snapshot_description.html');
        $this->assertNotEmpty($html);

        $parser = new ProductPageParser($html);
        $product = $parser->getProduct();
        $this->assertNotEmpty($product);
        $this->assertInstanceOf(Product::class, $product);

        /** @var Product $product */
        $this->assertEquals('Sainsbury\'s Ripe & Ready Golden Kiwi x4', $product->title);
        $this->assertEquals(204743, $product->size);
        $this->assertStringContainsString('Sweet & juicy', $product->description);
        $this->assertEquals(2, $product->unitPrice);
    }

    public function testGetProductSnapshotPromo()
    {
        $html = $this->readFixtureFile('product_page_snapshot_promo.html');
        $this->assertNotEmpty($html);

        $parser = new ProductPageParser($html);
        $product = $parser->getProduct();
        $this->assertNotEmpty($product);
        $this->assertInstanceOf(Product::class, $product);

        /** @var Product $product */
        $this->assertEquals('By Sainsbury’s 4 Small Ripe & Ready Avocados', $product->title);
        $this->assertEquals(918455, $product->size);
        $this->assertStringContainsString('Rich and creamy', $product->description);
        $this->assertEquals(2.25, $product->unitPrice);
    }

    public function testGetProductSnapshot()
    {
        $html = $this->readFixtureFile('product_page_snapshot.html');
        $this->assertNotEmpty($html);

        $parser = new ProductPageParser($html);
        $product = $parser->getProduct();
        $this->assertNotEmpty($product);
        $this->assertInstanceOf(Product::class, $product);

        /** @var Product $product */
        $this->assertEquals('By Sainsbury’s Medium Ripe & Ready Avocados x2', $product->title);
        $this->assertEquals(155789, $product->size);
        $this->assertStringContainsString('Rich and creamy', $product->description);
        $this->assertEquals(1.6, $product->unitPrice);
    }

}
