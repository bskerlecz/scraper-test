<?php

namespace App\Providers;

use App\Scraper\Http\HttpClientFactory;
use App\Scraper\Scraper;
use App\Scraper\ScraperInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HttpClientFactory::class, function ($app) {
            return new HttpClientFactory();
        });

        $this->app->bind(ScraperInterface::class, Scraper::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides(): array
    {
        return [
            HttpClientFactory::class,
            ScraperInterface::class
        ];
    }
}
