<?php

namespace Tests\Unit\Scraper\Sources\Sainsbury\Parsers;

use App\Scraper\Sources\Sainsbury\Parsers\CategoryPageParser;
use PHPUnit\Framework\TestCase;
use Tests\ReadFixtureFile;

/**
 * Class CategoryPageParserTest
 *
 * Sainsbury Ripe Category page parser test
 *
 * @package Tests\Unit\Scraper\Sources\Sainsbury\Parsers
 */
class CategoryPageParserTest extends TestCase
{
    use ReadFixtureFile;

    public function testGetProductPageUrlsSnapshot() {
        $html = $this->readFixtureFile('category_page_snapshot.html');
        $this->assertNotEmpty($html);

        $parser = new CategoryPageParser($html);
        $urls = $parser->getProductPageUrls();

        $this->assertNotEmpty($urls);
        $this->assertCount(14, $urls);

        $url1 = $urls[0];
        $this->assertEquals(
            'https://www.sainsburys.co.uk/shop/gb/groceries/product/details/ripe---ready/sainsburys-avocado--ripe---ready-x2',
            $url1
        );
    }

}
