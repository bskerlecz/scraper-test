<?php


namespace App\Scraper\Common;

class MissingParameterException extends \Exception
{

    /**
     * MissingParameterException constructor.
     * @param string $parameterName
     */
    public function __construct(string $parameterName)
    {
        parent::__construct("Required parameter '{$parameterName}' is missing or empty.");
    }
}
