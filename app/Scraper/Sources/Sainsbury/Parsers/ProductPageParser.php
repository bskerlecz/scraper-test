<?php


namespace App\Scraper\Sources\Sainsbury\Parsers;


use App\Models\Product;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ProductPageParser
 *
 * Parse basic product info from a Sainsbury product page
 *
 * @package App\Scraper\Sources\Sainsbury\Parsers
 */
class ProductPageParser
{
    protected $html;
    protected $product;

    /**
     * ProductPageParser constructor.
     * @param $html
     */
    public function __construct($html)
    {
        $this->html = $html;
    }

    /**
     * Remove any alphanumeric information (eg. currency) from the price value
     *
     * @param $priceStr
     * @return float
     */
    protected function cleanPrice($priceStr): float
    {
        $price = preg_replace("/[^0-9\.]/", "", $priceStr);
        return floatval($price);
    }

    /**
     * Parse and return basic product information
     *
     * @return Product|false
     */
    public function getProduct()
    {
        if ($this->product == null) {
            $crawler = new Crawler();
            $crawler->addHtmlContent($this->html);

            try {
                $size = strlen($this->html);
                $title = $crawler->filter('.product-details-page .pd__right h1.pd__header')->text();
                $description = $crawler->filter('.product-details-page .pd-details .productText')->text();
                $unit_price = $crawler->filter('.product-details-page .pd__right .pd__cost > div')->text();

                $product = new Product([
                    'title' => trim($title),
                    'unitPrice' => $this->cleanPrice($unit_price),
                    'description' => trim($description),
                    'size' => $size
                ]);

                $this->product = $product;
            } catch (\Exception $e) {
                Log::warning("Page parsing failed: '{$e->getMessage()}' - {$e->getTraceAsString()}", [__METHOD__]);
                $this->product = false;
            }
        }

        return $this->product;
    }

}
